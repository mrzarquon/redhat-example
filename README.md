# redhat-example

This example uses Gitpod's custom [workspace](https://www.gitpod.io/docs/configure/workspaces/workspace-image) dockerfile option to build a basic redhat base image.

```yaml
image:
  file: .gitpod.Dockerfile
```

Is all that is in the .gitpod.yml file

Redhat 8 is the default in the [.gitpod.Dockerfile](.gitpod.Dockerfile) and there are examples for [.gitpod.rhel9.Dockerfile](.gitpod.rhel9.Dockerfile) and [.gitpod.centos7.Dockerfile](.gitpod.centos7.Dockerfile)

If you are doing a lot of customization of these images (these are the bare minimum images for gitpod + tailscale), especially centos 7, which is EOLed, it would be better to move the image creation process to a separate repository with a scheduled CI job to ensure they are updated, and have it publish private images for your use. You can refer to the [Private Image](https://gitlab.com/mrzarquon/private-image) repository for instructions on how to use those privately built images. The built process is identical to any Dockerfile creation, so is not documented here.

