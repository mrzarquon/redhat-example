FROM docker.io/library/centos:centos7

USER root


RUN yum install -y yum-utils &&\
    yum-config-manager --add-repo https://pkgs.tailscale.com/stable/centos/7/tailscale.repo &&\
    yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo &&\
    yum install -y epel-release ncurses sudo tailscale docker-ce docker-ce-cli containerd.io docker-compose-plugin llvm-toolset &&\
    rpm -U  https://repo.ius.io/ius-release-el7.rpm &&\
    yum install -y git236 &&\
    rm -rf /var/cache/yum

RUN useradd -l -u 33333 -G wheel -md /home/gitpod -s /bin/bash -p gitpod gitpod \
    && sed -i.bkp -e 's/%wheel\s\+ALL=(ALL\(:ALL\)\?)\s\+ALL/%wheel ALL=NOPASSWD:ALL/g' /etc/sudoers

USER gitpod
