
FROM docker.io/redhat/ubi9:latest

USER root


RUN dnf config-manager --add-repo https://pkgs.tailscale.com/stable/rhel/9/tailscale.repo \
    && dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo \
    && dnf install -y ncurses dnf-plugins-core git-core sudo tailscale docker-ce docker-ce-cli containerd.io docker-compose-plugin llvm-toolset\
    && dnf clean all \
    && rm -rf /var/cache/yum

RUN useradd -l -u 33333 -G wheel -md /home/gitpod -s /bin/bash -p gitpod gitpod \
    && sed -i.bkp -e 's/%wheel\s\+ALL=(ALL\(:ALL\)\?)\s\+ALL/%wheel ALL=NOPASSWD:ALL/g' /etc/sudoers

USER gitpod
